using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

public class CoordinatesOpen : MonoBehaviour {

	// 0.05 seconds interval
	float interval = 0.05f;
	string path;
	int lasti = 0;
	int lastj = 0;
	public static List<string> fileLines = new List<string>();
	public static List<float> yaw = new List<float>();
	public static List<float> pitch = new List<float>();
	public static List<float> roll = new List<float>();
	public static List<float> x = new List<float>();
	public static List<float> y = new List<float>();
	public static List<float> z = new List<float>();


	// Use this for initialization
	void Start () {
		InvokeRepeating("UpdateInterval",interval,interval);

		path = "/mnt/sdcard/";
		// For debugging in Unity editor uncomment the next line and put abc.txt in Assets folder
        //path = Application.dataPath+"/";


	}
	void UpdateInterval()
	{

		using ( StreamReader sr = new StreamReader(path+"abc.txt"))
		{
			
			// Get Number of Valid Lines starting with ypr
			int i = 0;
			// Total Number of Lines
			int j = 0;
			while (sr.ReadLine() != null && j<lastj) { 
				j++; 
			}
			
			//	Now only new lines are there
			string line;
			while ((line = sr.ReadLine()) != null) { 
				j++;
				if(line.StartsWith("ypr")){
					fileLines.Add(line);

					//Regex Based Search for Numbers
					string pattern = @"-?\d*\.{0,1}\d+";
					string input = line;

					MatchCollection matches = Regex.Matches(input, pattern);
					int k=0;
					Debug.Log(matches.Count);
					if(matches.Count==6){
						foreach (Match match in matches)
						{
							switch(k)
							{
								case 0:
									yaw.Add(float.Parse(match.Groups[0].Value));
									break;
								case 1:
									pitch.Add (float.Parse(match.Groups[0].Value));
									break;
								case 2:
									roll.Add(float.Parse(match.Groups[0].Value));
									break;
								case 3:
									x.Add(float.Parse(match.Groups[0].Value));
									break;
								case 4:
									y.Add(float.Parse(match.Groups[0].Value));
									break;
								case 5:
									z.Add (float.Parse(match.Groups[0].Value));
									break;
							}
							k++;
						}
					}
					i++;
				}
			}

			lasti = i;
			lastj = j;
			Debug.Log ("OK");
			if (fileLines.Count>100	){
				// Display the last line
				//GetComponent<TextMesh>().text = fileLines[fileLines.Count-1];
				//string newtext = GetComponent<TextMesh>().text ;
				//newtext = newtext +" Size=" +fileLines.Count;
				//GetComponent<TextMesh>().text = newtext;
//				Debug.Log(fileLines[fileLines.Count-2]);
//				Debug.Log (fileLines.Count);
//				Debug.Log(yaw[fileLines.Count-2]+" "+pitch[fileLines.Count-2]+" "+roll[fileLines.Count-2]+" "+x[fileLines.Count-2]+" "+y[fileLines.Count-2]+" "+z[fileLines.Count-2]+" ");
//
			}
			
		} 

	}

}
