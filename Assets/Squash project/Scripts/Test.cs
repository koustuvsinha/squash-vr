﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		var x = CoordinatesOpen.x [CoordinatesOpen.x.Count - 1];
		var y = CoordinatesOpen.y [CoordinatesOpen.y.Count - 1];
		var z = CoordinatesOpen.z [CoordinatesOpen.z.Count - 1];

		Debug.Log ("x = " + x + ", y = " + y + ", z = " + z);
		// scaling
		// x = 0 to -4
		// y = 2 to 7
		// z = -5 to +5
		x = -(x / 250) + 1;
		y = -(y / 400) + 2 ;
		z = (z / 2920) + 1;

		transform.position = new Vector3(x,y,z);
	}
}
