﻿using UnityEngine;
using System.Collections;

public class MouseDrag : MonoBehaviour {

	public float distance;
	public int ballMass;
	public float offset;
	public float horizontalSpeed;
	public float verticalSpeed;

	void FixedUpdate() {
		//print (Input.mousePosition.x + "," + Input.mousePosition.y);
		Vector3 mousePosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, distance);
		
		Vector3 objectPosition = Camera.main.ScreenToWorldPoint (mousePosition);
		
		transform.position = objectPosition;
	}

	/*
	void FixedUpdate() {
		float h = horizontalSpeed * Input.GetAxis ("Mouse X");
		float v = verticalSpeed * Input.GetAxis ("Mouse Y");
		Rigidbody rb = GetComponent<Rigidbody> ();
		rb.MovePosition (rb.position + transform.rotation * new Vector3 (v, h, 0));
	}
	*/

	void OnCollisionEnter(Collision col) {
		if (col.gameObject.tag == "ball") {
			Debug.Log ("Strike");

			float force = col.relativeVelocity.sqrMagnitude * ballMass;

			Rigidbody rb = col.gameObject.GetComponent<Rigidbody>();
			rb.AddForce(transform.right * force);

		}
	}

}
