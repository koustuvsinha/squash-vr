﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	private Rigidbody rb;
	public float speed;
	public float maxSpeed = 200f;

	void Start () {
		rb = GetComponent<Rigidbody> ();

	}

	void Update() {
		if (Input.GetKey (KeyCode.S)) {
			rb.AddForce (transform.right * speed);
			rb.useGravity = true;		
		}
		if (Input.GetKey (KeyCode.W)) {
			rb.AddForce (transform.up * speed);
			rb.useGravity = true;
		}
		if(Input.GetKey(KeyCode.Q)) {
			rb.AddForce (transform.right * 1000);
		}
	
	}

	void FixedUpdate() {
		if(rb.velocity.magnitude > maxSpeed)
		{
			rb.velocity = rb.velocity.normalized * maxSpeed;
		}
	}

}
